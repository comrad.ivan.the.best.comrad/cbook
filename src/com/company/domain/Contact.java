package com.company.domain;

import com.company.database.DataBase;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class Contact {
    private String name;
    private String surname;
    private ArrayList<Phone> phones = new ArrayList<>();
    private ArrayList<Email> emails = new ArrayList<>();
    private LocalDateTime createDate;
    private LocalDateTime birthday;

    Contact(String name){
        this.name = name;
        createDate = LocalDateTime.now();
    }
    public Contact(String name, String surname){
        this(name);
        this.surname = surname;
        DataBase.writeSQL(
                "contact",
                "name, surname",
                getString());
    }

    private String getString(){
        return this.name + "', '" + this.surname;
    }

    public void setName(String name){this.name = name;}
    public String getName(){return this.name;}
    public void setSurname(String surname){this.surname = surname;}
    public String getSurname(){return this.surname;}
    public LocalDateTime getCreateDate(){return this.createDate;}
    public void setBirthday(LocalDateTime date){this.birthday = date;}
    public LocalDateTime getBirthday(){return this.birthday;}

    public void addPhone(Phone phone){
        this.phones.add(phone);
    }
    public void addEmail(Email email){
        this.emails.add(email);
    }
}
