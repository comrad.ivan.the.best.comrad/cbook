package com.company.app.components;

import com.company.domain.Contact;
import com.company.domain.Email;
import com.company.domain.Phone;
import com.company.error.FormError;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

public class AddContact extends JPanel {
    private static JTextField name;
    private static JTextField surname;
    private static JTextField phone;
    private static JTextField email;
    private static JLabel error = new JLabel("");
    public AddContact(){
        add(error);
        name = addTextField("Введите имя", this);
        surname = addTextField("Введите фамилию", this);
        phone = addTextField("Введите телефон", this);
        email = addTextField("Введите email", this);

        JButton button = addButtonSave("Сохранить", this);
    }

    private JButton addButtonSave(String title, JComponent component) {
        JButton button = new JButton("Сохранить");
        component.add(button);
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                FormError.list.clear();
                Phone userPhone = new Phone(phone.getText());
                Email userEmail = new Email(email.getText());

                if (FormError.list.size() > 0)
                    error.setText(FormError.list.get(0));
                else {
                    Contact user = new Contact(
                            name.getText(),
                            surname.getText()
                    );
                    user.addPhone(userPhone);
                    user.addEmail(userEmail);
                    error.setText("");
                }
            }
        });

        return button;
    }

    private JTextField addTextField(String title, JComponent component){
        JTextField field = new JTextField(title);
        field.setText(title);
        component.add(field);
        field.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                if (field.getText().equals(title))
                    field.setText("");
            }
            @Override
            public void focusLost(FocusEvent e) {
                if (field.getText().equals(""))
                    field.setText(title);
            }
        });
        return field;
    }
}
