package com.company.app.components;

import javax.swing.*;
import java.awt.*;

public class MainMenu extends JMenuBar {
    public MainMenu(){
        JMenu file = addMenu("Файл", this);
        JMenu newFile = addMenu("Новый", file);
        JMenuItem newContact = addMenuItem("Контакт", newFile);
        JMenuItem newImage = addMenuItem("Картинка", newFile);
        JMenuItem close = addMenuItem("Закрыть", file);

        JMenu edit = addMenu("Правка", this);
        JMenu operation = addMenu("Операции", edit);
        JMenuItem copy = addMenuItem("Копировать", operation);
        JMenuItem cut = addMenuItem("Вырезать", operation);
        JMenuItem paste = addMenuItem("Вставить", operation);

        JMenu help = addMenu("Помощь", this);
    }

    public JMenu addMenu(String name, JComponent parent){
        Font font = new Font("Verdana", Font.PLAIN, 14);
        JMenu menu = new JMenu(name);
        menu.setFont(font);
        parent.add(menu);
        return menu;
    }

    public JMenuItem addMenuItem(String name, JComponent parent){
        Font font = new Font("Verdana", Font.PLAIN, 12);
        JMenuItem menuItem = new JMenuItem(name);
        menuItem.setFont(font);
        parent.add(menuItem);
        return menuItem;
    }
}
