package com.company.app;

import com.company.app.components.AddContact;
import com.company.app.components.MainMenu;

import javax.swing.*;

public class MainFrame extends JFrame {
    public MainFrame(){
        setTitle("CBooK");
        setSize(500, 500);
        setJMenuBar(new MainMenu());

        add(new AddContact());

        setLocation(200, 300);
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
}
