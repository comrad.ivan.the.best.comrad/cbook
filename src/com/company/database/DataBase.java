package com.company.database;

import java.sql.*;

public class DataBase {
    public static void writeSQL(String table, String params, String values){
        try {
            Class.forName(dbDriver);

            connection = DriverManager.getConnection(dbURL, user, pass);
            statement = connection.createStatement();
            String sql = String.format("INSERT INTO %s(%s) VALUES ('%s')", table, params, values);

            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.executeUpdate();

            connection.close();
        } catch (ClassNotFoundException | SQLException e){
            e.printStackTrace();
        }
    }

    public static void readSQL(String table, String params){
        String[] colunm = params.replace(",", "").split(" ");
        try {
            Class.forName(dbDriver);
            connection = DriverManager.getConnection(dbURL, user, pass);
            statement = connection.createStatement();

            String sql = String.format("select %s from %s", params, table);
            resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {
                for (String p : colunm)
                    System.out.print(p + ": " + resultSet.getString(p) + ", ");
                System.out.println();
            }

            connection.close();
        } catch (ClassNotFoundException | SQLException e){
            e.printStackTrace();
        }
    }

    public static final String dbDriver = "com.mysql.cj.jdbc.Driver";
    public static final String dbURL = "jdbc:mysql://194.67.78.87:3306/cbook";
    public static String user = "cbook";
    public static String pass = "Qwerty123";

    private static Connection connection = null;
    private static Statement statement = null;
    private static ResultSet resultSet = null;
}
